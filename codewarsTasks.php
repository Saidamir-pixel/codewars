<?php 

function solution1($a, $b){
    $result = [];
    $final = [];

    for($i=0;$i<count($a);$i++){
        array_push($result,  ($a[$i] - $b[$i]) **2);
        if (count($result) == count($a)){
            array_push($final, array_sum($result));
        }
    }
    print_r($final[0] / count($result));
}

solution1([1,2,3],[4,5,6]);
echo '</br>';



function solution2($a, $b)
{
  $result1 = [];
  $result1Sum = [];
  
  $result2 = [];
  $result2Sum = [];
  
  for($i=0;$i<count($a);$i++){
    array_push($result1, $a[$i]);
    array_push($result1Sum, array_sum($result1));
  }
  
  for($y=0;$y<count($b);$y++){
    array_push($result2, $b[$y]);
    array_push($result2Sum, array_sum($result2));  
  }
  
  for($x=0;$x<count($result2Sum);$x++){
    if(in_array($b[$x], $result1Sum)){
      print_r($b[$x]);
      break;
    }
  }
    
}

$array1 = [1, 2, 3, 4];
$array2 = [3, 2, 1];

solution2($array1, $array2);
echo '</br>';




function solution3($arr){
  $min = $arr[0];
  $max = $arr[0];

  foreach($arr as $val){
    if($val < $min){
      $min = $val;
    }
  }

  foreach($arr as $val){
    if($val > $max){
      $max = $val;
    }
  }

  print_r($min);
  echo '<br>';
  print_r($max);
}

solution3([1, 2, 3, 4, 5, 6]);
echo '</br>';




function solution4($arr){
  $result = [];
  for($i=0;$i<count($arr);$i++){
    if($arr[$i] < 0){
      array_push($result, $arr[$i] * -1);
    }else{
      array_push($result, $arr[$i] * -1);
    }
  }
  echo '<pre>';
  print_r($result);
  echo '<pre>';
}

solution4([1,2,3,-4,5]);
echo '</br>';




function my($number){
  $base1 = ['('];
  $base2 = [') '];
  $base3 = ['-'];
  $result = [];
  for($i=0;$i<3;$i++){
    array_push($base1, $number[$i]);
  }
  for($i=3;$i<6;$i++){
    array_push($base2, $number[$i]);
  }
  for($i=6;$i<10;$i++){
    array_push($base3, $number[$i]);
  }

  $result = array_merge($base1, $base2, $base3);
  for($x=0;$x<count($result);$x++){
    print_r($result[$x]);
  }
  
  
}
my([1,2,2,3,6,4,2,6,9,0]);
echo '</br>';






function square_digits($num){
  $result = [];
  for($i=0;$i<count($num);$i++){
    array_push($result, $num[$i] ** 2);
  }
  for($i=0;$i<count($num);$i++){
    print_r($result[$i].' ');
  }
}

square_digits([9,9,9]);
echo '</br>';




function dutyFree(int $d, int $h) {
   $dPrice = ($d*$h) / 100;
   print_r($dPrice);
}
dutyFree(10, 500);
echo '</br>';




function divisors($n) {
  if($n < 0){
    return false;
  }
  $result = [];
  for($i=1;$i<=$n;$i++){
    if($n % $i == 0){
      array_push($result, $i);
    }
  }
  print_r(count($result));
}

divisors(10);
echo '</br>';



function narcissistic(int $value) {
    $result = [];
    $digits = str_split($value);
    for($i=0;$i<count($digits);$i++){
      array_push($result, $digits[$i]**count($digits));
    }
    if(array_sum($result) == $value){ 
      print_r('true');
    }else{
      print_r('false');
    }
  }
  
narcissistic(153);
echo '</br>';



function generateHashtag($str) {
  if(mb_strlen($str) > 140){
    return false;
  }
  $newStr = mb_convert_case($str, MB_CASE_TITLE, "UTF-8");
  $newStr = str_replace(" ", '', $newStr);
  if(mb_strlen($newStr) == 0){
    return false;
  }else{
    $newStr = '#'.$newStr;
    print_r($newStr);
  }
}

generateHashtag("Lorem ipsum dolor sit amet consectetur adipisicing elit.");
echo '</br>';




function descendingOrder(int $n) {
  $result = [];
  $digits = str_split($n);
  for($i=count($digits) - 1;$i>=0;$i--){
    array_push($result, $digits[$i]);
  }
  $strR = implode($result);
  $intR = (int)$strR;
  print_r($intR);
}

descendingOrder(3103721347);
echo '</br>';


  
function number($bus_stops) {
  $result = [];

  for($i=0;$i<$bus_stops;$i++){
      if($result[$i] == $result[1]){
        array_push($result, [random_int(0, 10)]);
        array_push($result[$i], random_int(0, $result[$i][0]));
        $sum = $result[$i][0] - $result[$i][1];
        array_push($result[$i], $sum);
      }else{
        array_push($result, [random_int(0, 10)]);
        array_push($result[$i], random_int(0, $result[$i][0]));
        $sum = $result[$i][0] - $result[$i][1];
        array_push($result[$i], $sum);
      }
  }
  echo '<pre>';
  print_r($result);

  echo '<pre>';
  
}

number(10);
echo '</br>';
